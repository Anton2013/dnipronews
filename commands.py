#!venv/bin/python3
from flask.ext.script import Manager

from extensions import app
from helpers import wrote_news_into_db

manager = Manager(app)

@manager.command
def get_news():
    all_links = wrote_news_into_db()
    print('{0} news added!'.format(len(all_links)))


if __name__ == "__main__":
    manager.run()