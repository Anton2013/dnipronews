#!venv/bin/python3
import datetime

from flask import render_template, redirect, url_for, jsonify

from settings import DEBUG
from extensions import app, db
from models import News
from helpers import wrote_news_into_db


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/news/', methods=['GET', 'POST'])
def start_build():
    all_links = wrote_news_into_db()
    today = datetime.datetime.now().date()
    return render_template('links.html', **{'news_date': today.strftime('%Y-%m-%d')})


@app.route('/archive/', methods=['GET'])
def archive():
    results = db.session.query(News.pub_date.distinct())
    dates_list = [x[0].strftime('%Y-%m-%d') for x in results]
    return render_template('archive.html', **{'dates_list': dates_list})


@app.route('/archive/<news_date>/')
def archive_date(news_date):
    try:
        news_date = datetime.datetime.strptime(news_date, '%Y-%m-%d')
    except ValueError:
        return redirect(url_for('home'))
    else:
        return render_template('links.html', **{'news_date': news_date.strftime('%Y-%m-%d')})

@app.route('/api/archive/<news_date>/')
def api_archive_date(news_date):
    try:
        news_date = datetime.datetime.strptime(news_date, '%Y-%m-%d')
    except ValueError:
        return redirect(url_for('home'))
    else:
        news = News.query.filter_by(pub_date=news_date.date()).order_by('-id')
        links = [{'id': n.title, 'link': "<a href='{0}'>{1}</a>".format(n.link, n.title)} for n in news]
        return jsonify(**{'data': links})

if __name__ == '__main__':
    app.debug = DEBUG
    app.run()
