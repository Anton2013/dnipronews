import requests
import datetime
import locale

import bs4
import html5lib

locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')


class VgorodeScrapper(object):
    urls = ['http://dp.vgorode.ua/news/', 'http://dp.vgorode.ua/news/tag/8301-obzory']
    news_links = []
    soup = bs4.BeautifulSoup

    def __init__(self):
        super(VgorodeScrapper, self).__init__()
        yesterday = datetime.datetime.today() - datetime.timedelta(days=1)
        self.n_day = yesterday.strftime('%e')
        self.n_month = yesterday.strftime('%B')

    @classmethod
    def process_links(cls, n):
        return n.find('div', class_='title').a

    def build_links(self):
        for url in self.urls:
            response = requests.get(url)
            soup = self.soup(response.text,'html5lib')
            news = soup.find_all('article')
            news_links = list(map(VgorodeScrapper.process_links, news))
            self.news_links = [x for x in news_links if x]
        return self.news_links


class Ua0562Scrapper(object):
    url = 'http://www.056.ua/news/search?period=day'
    news_links = []
    soup = bs4.BeautifulSoup
    only_news_block = bs4.SoupStrainer(class_='news_list')

    @classmethod
    def process_links(cls, n):
        if n.attrs.get('href', ''):
            n.attrs['href'] = 'http://www.056.ua{0}'.format(n.attrs['href'])
            return n

    def build_links(self):
        response = requests.get(self.url)
        soup = self.soup(response.text, 'html5lib', parse_only=self.only_news_block)
        news = soup.find_all(class_='namelink')
        news_links = list(map(Ua0562Scrapper.process_links, news))
        self.news_links = news_links if any(news_links) else []
        return self.news_links

