from extensions import db
from scraper import VgorodeScrapper, Ua0562Scrapper
from models import News


def wrote_news_into_db():
    vgorode = VgorodeScrapper()
    ua = Ua0562Scrapper()
    all_links = []
    try:
        ua_links = ua.build_links()
    except:
        pass
    else:
        for link in ua_links:
            title = list(link.stripped_strings)[0]
            if not News.query.filter_by(title=title).count():
                href = link.attrs['href']
                db.session.add(News(title, href))
                all_links.append(link)
    try:
        links = vgorode.build_links()
    except:
        pass
    else:
        for link in links:
            title = list(link.stripped_strings)[0]
            if not News.query.filter_by(title=title).count():
                href = link.attrs['href']
                db.session.add(News(title, href))
                all_links.append(link)
    db.session.commit()
    return all_links