import datetime

from extensions import db


class News(db.Model):
    __tablename__ = 'news'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256))
    link = db.Column(db.Text)
    pub_date = db.Column(db.Date)

    def __init__(self, title, link, pub_date=None):
        self.title = title
        self.link = link
        if pub_date is None:
            pub_date = datetime.datetime.now().date()
        self.pub_date = pub_date