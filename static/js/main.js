var React = require('react');
var ReactDOM = require('react-dom');

var News = React.createClass({
    render: function () {
        return <div className='col-xs-12 text-xs-center'
                    dangerouslySetInnerHTML={{__html: this.props.data}}>
        </div>
    }
});


var NewsList = React.createClass({
    componentDidMount: function () {
        this.serverRequest = $.get(this.props.url, function (data) {
            this.setState({data: data.data});
        }.bind(this));
    },
    componentWillUnmount: function () {
        this.serverRequest.abort();
    },
    getInitialState: function () {
        return {data: [{id:'waiting', link:'<p>Waiting news</p>'}]};
    },
    render: function () {
        return (
            <article>
                {this.state.data.map(function (res) {
                    return <News key={res.id} data={res.link} />
                })}
            </article>
        )
    }
});

main = {
    links: function (options) {
        ReactDOM.render(
            <NewsList url={options.links_url} />,
            document.getElementById('links-container')
        );
    }
};